/* --- VARIABLES --- */
// FILTERS - GEOLOCATION
var fGeolocation = window.localStorage.getItem("filters-geolocation") ? window.localStorage.getItem("filters-geolocation") : 'on';
$('#flip-geolocation').val(fGeolocation);

var prevSelection = "overview";
$("#navbar ul li").on("click",function(){
    var newSelection = $(this).children("a").attr("data-tab-class");
    $("."+prevSelection).addClass("ui-screen-hidden");
    $("."+newSelection).removeClass("ui-screen-hidden");
    prevSelection = newSelection;
});

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}

function fillSearch(){
	console.log("populating search input");
	var query = window.localStorage.getItem("search-query");
	$("#search").val(query);
	//$("#search").trigger("keyup");
}


function search()
{
	console.log("fGeolocation: "+fGeolocation);
	console.log("searching");
	// Hide loading widget
	$.mobile.loading( 'show' );

	var $input = $("#search");
	var value = $input.val();

	var data = {};

	if ( (value && value.length > 2) ) {
		// $ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
		// $ul.listview( "refresh" );

		data['query'] = value;
		
		if(fGeolocation == 'on') {
			getCoords().done(function (coords) {

				if(fGeolocation == 'on')
					data['geolocation'] = coords.latitude +", " + coords.longitude;
				performRequest(data);
			   
			}).fail(function () {
			    console.log('failed');
			});
		} else {
			performRequest(data);
		}
	}
}

function performRequest(data) {
	 $.ajax({
	    url: "https://api.eet.nu/venues",
	    dataType: "jsonp",
	    crossDomain: true,
	    data: data
	})
	.then( function ( response ) {
		var $ul = $( "#results" );
		$ul.html( "" );
		var html = "";
		if(response["results"].length > 0){
			$.each( response["results"], function ( i, val ) {

				// save venue ID to localStorage
				window.localStorage.setItem("venue-id", val.id);

				var cShort = (val.images.original[0] != null) ? " short" : "";
				html += "<li><a href='#page_details?id=" + val.id + "'><h2 class='result-header" + cShort + "'>" + val.name + "</h2>";

				if(val.category != null) {
					html += "<p class='result-body" + cShort + "'>" + val.category + "</p>";
				} else {
					html += "<p class='result-body" + cShort + "'></p>";
				}
				if(val.tagline != null) {
					html += "<p class='result-misc" + cShort + "'>" + val.tagline + "</p>";
				} else {
					html += "<p class='result-misc" + cShort + "'></p>";
				}
				if(val.images.original[0] != null)
					html += "<p class='result-image ui-li-aside'><img src='" + val.images.original[0] + "' /></p>";
				if(val.rating != null){
					html += '<div class="result-rating"><i class="fa fa-star"></i>' + '&nbsp;' + ( val.rating / 10 ) + '</div>';
				}
				html += "</a></li>";
			});
		} else {
			html += "<li>No results available.</li>";
		}
		// Hide loading widget
		$.mobile.loading( 'hide' );
        
		$ul.html( html );
        $ul.listview( "refresh" );
        $ul.trigger( "updatelayout");
	});
}

function getCoords() {

    var coords = 5;
    var deferred = $.Deferred();

    navigator.geolocation.getCurrentPosition(function (position) {
        deferred.resolve({
            longitude: position.coords.longitude,
            latitude: position.coords.latitude,
        });
    }, function (error) {
        deferred.reject();
    });

    return deferred.promise();
}