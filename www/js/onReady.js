$(function () {
    $("[data-role=header]").toolbar();
    $("[data-role=panel]").panel().enhanceWithin();

    // Back button
    $(document).on('pagebeforeshow', function () {

        var activePage = $.mobile.activePage;
        $("#btn_back").show();
        $("#navbar").show();
        if( activePage[0].id == "page_home" ) {
            $("#btn_back").hide();
            $("#navbar").hide();
        }
    });

    $("a.url").on("click", function(event){
        var ref = window.open(encodeURI($(this).attr("href")), '_system');
        event.preventDefault();
    });
    // Bind the swipeleftHandler callback function to the swipe event on div.box
    $( "div.overview" ).on( "swiperight", swiperightHandler );
    $( "div.reviews" ).on( "swiperight", swiperightHandler );
     
    // Callback function references the event target and adds the 'swipeleft' class to it
    function swiperightHandler( event ){
        //history.back();
        $.mobile.changePage("#page_home");
        event.preventDefault();
    }
});

$( document ).on( 'change', '#flip-geolocation', function( e ) {
        console.log("change geolocation");

    /*if(fGeolocation!==$(this).val()){
        alert($(this).val());
    }*/
    fGeolocation = $(this).val();
    window.localStorage.setItem("filters-geolocation", fGeolocation);
    search();
});

window.addEventListener('load', function () { 
    console.log("load");

    // Search query
    if( $("#search").val() != "")
        search()

    document.addEventListener("deviceready", function () { 
        document.addEventListener("pause", function () {
            //alert("pausing");
        });
        document.addEventListener("resume", function () {
            console.log("resuming");
            //fillSearch();
            //search();
        });
    }); 
}, false);


// Listen for any attempts to call changePage().
$(document).bind( "pagebeforechange", function( e, data ) {
    // We only want to handle changePage() calls where the caller is
    // asking us to load a page by URL.
    if ( typeof data.toPage === "string" ) {
        // We are being asked to load a page by URL, but we only
        // want to handle URLs that request the data for a specific
        // category.
        var u = $.mobile.path.parseUrl( data.toPage ),
            re = /^#page_details/;

        if ( u.hash.search(re) !== -1 ) { // on details page
            console.log("loading details data");

            var id = u.hash.replace( /.*id=/, "" );
            if( $.isNumeric( id ) ){
                window.localStorage.setItem("id", id);
            } else {
                id = window.localStorage.getItem("id");
            }

            // GET DATA
            $.ajax({
                url: "https://api.eet.nu/venues/" + id,
                dataType: "jsonp",
                crossDomain: true,
                data: {}
            })
            .then( function ( response ) {
                // Populate view
                $( "#details_name" ).html(response.name + " in " + response.address.city);
                $( "#details_category" ).html(response.category);
                $( "#details_tagline" ).html(response.tagline);
                $( "#details-rating-stars").html('<span class="stars-' + Math.floor(response.rating / 10) + '"></span>');
                $( "#details-rating-value").html(response.rating / 10);
                /*$( "#details_rating").parent().html('<span id="details_rating"></span>');
                $( "#details_rating").raty({ readOnly: true, score: response.rating });
                $( "#details_rating").after(response.rating / 10);*/
                $( "#details_address" ).html(response.address.street + "<br />" + response.address.zipcode + " " + response.address.city + "<br />" + response.address.country);
                $( "#details_telephone" ).attr("href", "tel:" + response.telephone);
                $( "#details_telephone_sms" ).attr("href", "sms:" + response.telephone);
                $( "#details_website" ).html(response.website_url);
                $( "#details_website" ).attr("href", response.website_url);
                //$( "#details_website" ).attr("onclick", "window.open('" + response.website_url + "', '_system');");
                $( "#details_email" ).attr("href", response.url + "/contact");
            });


            $.ajax({
                url: "https://api.eet.nu/venues/" + id + "/reviews",
                dataType: "jsonp",
                crossDomain: true,
                data: {}
            })
            .then( function ( response ) {
                var html = "";
                if(response["results"].length > 0){
                    $.each( response["results"], function ( i, val ) {
                        var res = new Date(val.created_at).toUTCString();

                        html += "<div class=\"review-header ui-bar ui-bar-a ui-corner-all\"><div>" + val.author.name + "</div><span class=\"review-date\">" + res.substr(0, res.lastIndexOf(":")) + "</span>";
                        html += '<p class="rating average-rating" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating"><span class="value" itemprop="ratingValue">' + (val.rating / 10) + '</span></p>';
                        html += "</div>";
                        html += "<div class=\"review-body ui-body ui-body-a ui-corner-all\"><p>";
                        html += val.body + "<br>";

                        var sFood = (val.scores.food == null) ? "-" : (val.scores.food / 10);
                        var sAmbiance = (val.scores.ambiance == null) ? "-" : (val.scores.ambiance / 10);
                        var sService = (val.scores.service == null) ? "-" : (val.scores.service / 10);
                        var sValue = (val.scores.value == null) ? "-" : (val.scores.value / 10);
                        html += '<ul class="review-scores"><li class="score-food"><span>Eten &amp; drinken</span><strong>' + sFood + '</strong></li><li class="score-ambiance"><span>Sfeer</span><strong>' + sAmbiance + '</strong></li><li class="score-service"><span>Service</span><strong>' + sService + '</strong></li><li class="score-value"><span>Prijs / kwaliteit</span><strong>' + sValue + '</strong></li></ul>';
                        html += "</p></div>";
                    });
                } else {
                    html += "<div>No reviews available.</div>";
                }
                $("#reviews").html(html);
            });
        } else { // on home page
            //fillSearch();
        }
    }
});

/* --- EVENTS --- */
$( "#search" ).on('keyup', function() {
    console.log("saving query...");
    window.localStorage.setItem("search-query", $(this).val());
    search();
});