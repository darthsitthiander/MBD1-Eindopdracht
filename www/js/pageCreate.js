$( document ).on( "pagecreate", function() {
	// Flipswitch - Theme
	$("#flip-theme").on("change", function(event){
		var themeClass = $(this).val();

		$( "#page_home" ).removeClass( "ui-page-theme-a ui-page-theme-b" ).addClass( "ui-page-theme-" + themeClass );
		$( "#page_details" ).removeClass( "ui-page-theme-a ui-page-theme-b" ).addClass( "ui-page-theme-" + themeClass );
		$( "#ui-body-test" ).removeClass( "ui-body-a ui-body-b" ).addClass( "ui-body-" + themeClass );
		$( "#ui-bar-test, #ui-bar-form" ).removeClass( "ui-bar-a ui-bar-b" ).addClass( "ui-bar-" + themeClass );
		$( ".ui-collapsible-content" ).removeClass( "ui-body-a ui-body-b" ).addClass( "ui-body-" + themeClass );
		$( ".theme" ).text( themeClass );
	});
});
$( document ).on( "pagecreate", "#page_home", function() {
	fillSearch();
});